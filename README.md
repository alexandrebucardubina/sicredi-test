# sicredi-test

Projeto teste para Sicredi data 07/2020 utilizando Angular 9 (latest)

Executar comandos:

 - npm install 
 - npm start

 Acessar via browser http://localhost:4200

https://github.com/WoopSicredi/jobs/issues/6#issue-363608738


# Objetivo

Criar uma aplicação que contenha​:

Página de login:

Única página disponível se não estiver logado;

Criar um usuário básico para acesso.

Uma página de lista de dragões:

Os nomes devem estar em ordem alfabética;

A partir da lista, deverá ser possível remover e alterar as informações dos dragões.

Uma página com os detalhes de um dragão específico:

Os seguintes dados devem ser apresentados na página:

Data de criação; 

Nome; 

Tipo. 

Uma página para cadastro de dragões: 

Regras:

Layout responsivo;
Utilizar Stencil, React, Angular 6+ ou Web Components;
Usar um sistema de controle de versão para entregar o teste (Github, Bitbucket, ...).
O que será avaliado:

Organização do código;
Componentização das páginas;
Interface organizada e amigável;
Uso adequado do css/sass/less. NÃO é permitido usar bibliotecas de estilos como: bootstrap, material design, etc.
API
http://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/dragon

GET .../api/v1/dragon
lista de dragões

GET .../api/v1/dragon/:id
detalhes de um draão

POST .../api/v1/dragon
criação de um dragão

PUT .../api/v1/dragon/:id
edição de um dragão

DELETE .../api/v1/dragon/:id
deleção de um dragão

Bom teste!