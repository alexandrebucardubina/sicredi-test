import {Routes, RouterModule} from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import {AuthGuardRouterService} from "./services/auth-guard-router.service";
import { NgModule } from "@angular/core";
import { DragonsComponent } from "./components/dragons/dragons.component";
import { DragonsEdtComponent } from "./components/dragons/dragons-edt.component";

//NOTE:MODELO SIMPLES DE ROTA TAMBEM JA TRABALHEI COM SISTEMA MODULAR
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dragons',
    component: DragonsComponent,
    canActivate: [AuthGuardRouterService],
  },
  {
    path: 'dragons/new',
    component: DragonsEdtComponent,
    canActivate: [AuthGuardRouterService]
  },
  {
    path: 'dragons/:id',
    component: DragonsEdtComponent,
    canActivate: [AuthGuardRouterService]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

