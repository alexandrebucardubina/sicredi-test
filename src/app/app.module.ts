import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './services/login.service';
import { LocalStorageService } from './services/local-storage.service';
import { AuthGuardRouterService } from './services/auth-guard-router.service';
import { DragonsComponent } from './components/dragons/dragons.component';
import { DragonsService } from './services/dragons.service';
import { HttpClientModule } from '@angular/common/http'; 
import { DragonsEdtComponent } from './components/dragons/dragons-edt.component';
import { DatePipe } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DragonsComponent,
    DragonsEdtComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [LoginService, LocalStorageService, AuthGuardRouterService, DragonsService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
