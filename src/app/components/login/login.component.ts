import { Component, NgZone } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorLogin: Boolean = false;
  constructor(private fb: FormBuilder, private service: LoginService, private router: Router, private zone: NgZone) {
    this.form = this.fb.group({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
    this.router.navigate(['dragons']);
  }

  login() {
    if (this.form.valid) {
      this.service.login({ email: this.form.value['email'], password: this.form.value['password'] }).then(x => {
        this.zone.run(() => window.location.reload());
      }).catch(err => {
        console.log("aa", err);
        this.errorLogin = true
      })
    } else {
      Object.values(this.form.controls).forEach(control => {
        control.markAsDirty();
        control.markAsTouched();
      });
    }
  }

}
