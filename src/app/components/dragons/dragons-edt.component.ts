import { Component, OnInit } from '@angular/core';
import { DragonsService, IPagination } from '../../services/dragons.service';
import * as _ from 'lodash';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-dragons-edt',
  templateUrl: './dragons-edt.component.html',
  styleUrls: ['./dragons-edt.component.scss']
})
export class DragonsEdtComponent implements OnInit {
  form: FormGroup;
  isEdit: Boolean = false;
  id: String = null;
  constructor(private service: DragonsService, private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute, private datePipe: DatePipe) {
    this.form = this.fb.group({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required]),
      id: new FormControl('', []),
      createdAt: new FormControl(this.datePipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss'), [Validators.required])
    });
  }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params['id'];
    if (this.id) {
      this.isEdit = true;
      this.service.getEdit(this.id).then((x: any) => {
        this.form = this.fb.group({
          name: new FormControl(x.name, [Validators.required]),
          type: new FormControl(x.type, [Validators.required]),
          id: new FormControl(x.id, [Validators.required]),
          createdAt: new FormControl(this.datePipe.transform(x.createdAt, 'dd/MM/yyyy HH:mm:ss'), [Validators.required])
        });
      });
    }
  }

  save() {
    if (this.form.valid) {
      if (!this.isEdit) {
        this.service.save(this.form.value).then((x: any) => {
            this.router.navigate(['/dragons']);
        }).catch(x => alert("Ocorreu um erro ao salvar o dragão"));
      } else {
        this.service.saveEdt(this.form.value).then((x: any) => {
            this.router.navigate(['/dragons']);
        }).catch(x => alert("Ocorreu um erro ao salvar o dragão"));
      }
    } else {
      Object.values(this.form.controls).forEach(control => {
        control.markAsDirty();
        control.markAsTouched();
      });
    }
  }

}
