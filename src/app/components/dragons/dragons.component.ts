import { Component, OnInit, NgZone } from '@angular/core';
import { DragonsService, IPagination } from '../../services/dragons.service';
import * as _ from 'lodash';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dragons',
  templateUrl: './dragons.component.html',
  styleUrls: ['./dragons.component.scss']
})
export class DragonsComponent implements OnInit {
  pagination: IPagination = {
    page: 1,
    limit: 10,
    total: 0
  };
  items: any = [];
  constructor(private service: DragonsService, private localService: LocalStorageService, private router: Router, private zone: NgZone) {}

  ngOnInit() {
    this.search();
  }

  nextPage() {
    this.pagination.page = this.pagination.page + 1;
    this.search();
  }

  prevPage() {
    this.pagination.page = this.pagination.page - 1;
    this.search();
  }

  search = _.debounce(() => {
    this.service.get(this.pagination).then((x: any) => {
      this.items = _.orderBy(x, ['name', 'asc']);
    });
  }, 500);


  delete(op) {
    if (window.confirm("Você tem certeza que deseja deletar esse dragão?"))
      this.service.delete(op.id).then((x: any) => {
          this.search();
      }).catch((x: any) => {
        alert("Ocorreu  um erro na operação!");
      });
  }

  logoff() {
    this.localService.remove('user');
    this.zone.run(() => window.location.reload());
  }

}
