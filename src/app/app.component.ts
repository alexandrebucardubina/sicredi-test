import { Component, OnInit, NgZone } from '@angular/core';
import { LocalStorageService } from './services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(private localService: LocalStorageService, private router: Router, private zone: NgZone) {}
  title = 'Sicredi';
  isLogged: Boolean;
  ngOnInit() {
    this.zone.run((x: any) => {
      this.isLogged = (this.localService.get('user')) ? true : false;
    });
  }

  logoff() {
    this.localService.remove('user');
    this.zone.run(() => window.location.reload());
  }

}
