import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";
import { LoginService } from './login.service';

@Injectable()
export class AuthGuardRouterService implements CanActivate {

    constructor(private auth: LoginService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
        if (this.auth.user) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}
