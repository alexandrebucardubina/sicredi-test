import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
const API_ENDPOINT = environment.apiUrl;
export class IPagination {
    page: number;
    limit: number;
    total: number;
}
@Injectable()
export class DragonsService {
    private header = {
        headers: { 'Accept': 'application/json;' }
    };
    constructor(private http: HttpClient) { }

    get(pagination: IPagination) {
        let obj: any = Object.assign({
            params: {
                'page': pagination.page,
                'limit': pagination.limit
            }
        }, this.header);
        return this.http
            .get(`${API_ENDPOINT}dragon`, obj)
            .toPromise()
            .then((response: any) => {
                return response;
            });
    }
    delete(id) {
        return this.http
            .delete(`${API_ENDPOINT}dragon/${id}`, Object.assign({}, this.header))
            .toPromise()
            .then((response: any) => {
                return response;
            });
    }
    save(form: any) {
        return this.http
            .post(`${API_ENDPOINT}dragon`, form, Object.assign({}, this.header))
            .toPromise()
            .then((response: Response) => {
                return response;
            });
    }

    saveEdt(form: any) {
        let obj = {
            name: form.name,
            type: form.type
        };
        return this.http
            .put(`${API_ENDPOINT}dragon/${form.id}`, obj, Object.assign({}, this.header))
            .toPromise()
            .then((response: Response) => {
                return response;
            });
    }

    getEdit(id: String) {
        return this.http
            .get(`${API_ENDPOINT}dragon/${id}`, Object.assign({}, this.header))
            .toPromise()
            .then((response: any) => {
                return response;
            });
    }
}
