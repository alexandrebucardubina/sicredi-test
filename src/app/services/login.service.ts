import { Injectable } from '@angular/core';
import { LocalStorageService } from "./local-storage.service";
import { Router } from "@angular/router";

import * as _ from 'lodash';

const USER_KEY = 'user';
const PAR_KEY = 'parametros';
export class IUser {
    email: string;
    password: string;
}
const cLogins: IUser[] = [{ email: 'dragoes', password: '123' }];

@Injectable()
export class LoginService {
    public user: any;
    public parametros: any;
    constructor(
        private localStorage: LocalStorageService,
        private router: Router) {
        const userLocalStorage = this.localStorage.getObject(USER_KEY);
        this.user = userLocalStorage ? userLocalStorage : false;
        const parametrosLocalStorage = this.localStorage.getObject(PAR_KEY);
        this.parametros = parametrosLocalStorage ? parametrosLocalStorage : {};
    }
    login = ({ email, password }) => new Promise((resolve, reject) => {
        const user = _.find(cLogins, (x: IUser) => { return x.email == email && x.password == password; });
        if (user) {
            this.localStorage.setObject(USER_KEY, JSON.stringify(user.email));
            resolve(true);
        } else {
            reject(false);
        }
    })

    logout() {
        this.localStorage.remove(USER_KEY);
        this.router.navigate(['/login']);
    }
}
